# python -d solution_1.py
class Num:
    def __init__(self, n):
        self.num = n
    def __str__(self):
        return str(self.num)
class Add:
    def __init__(self, e1, e2):
        self.e1 = e1
        self.e2 = e2
    def __str__(self):
        return str(self.e1)+' + '+str(self.e2)
class Mul:
    def __init__(self, e1, e2):
        self.e1 = e1
        self.e2 = e2
    def __str__(self):
        return str(self.e1)+' * '+str(self.e2)
class Square:
    def __init__(self, e1):
        self.e1 = e1
    def __str__(self):
        return str(self.e1)+'^2'


def eval(n):
    if isinstance(n, Num):
        return n.num;
    elif isinstance(n, Add):
        return (eval(n.e1) + eval(n.e2))
    elif isinstance(n, Mul):
        return (eval(n.e1) * eval(n.e2))
    elif isinstance(n, Square):
        return (eval(n.e1)**2)
    else:
        raise Exception

def main():
    n0 = Mul(Add(Num(2), Num(3)), Num(4))
    n1 = Add(n0, n0)
    print n0
    print n1
    print eval(n0)
    print eval(n1)
    n3 = Add(Square(Num(2)), Square(Num(3)))
    n4 = Square(Num(5))
    print eval(n3)
    print n3
    print eval(n4)
    print n4

main()
