http://swish.swi-prolog.org/example/lists.pl
num(zero).
num(succ(N)) :- num(N).

not_member(_,[]).
not_member(X,[H|T]) :- X\=H, not_member(X,T).

not_repeated([]).
not_repeated([_]).
not_repeated([H|T]) :- not_member(T,H), repeated(T).



