def largestIncreasingSubstr(s):
    sub_strings = []
    current_sub_string = ''
    prev_char = ''
    index_count = 0
    if s == '':
        return ''

    # percorre todos os caracteres na string
    for prev_char in s:
        index_count += 1
        current_sub_string = prev_char
        # percorre string a partir de prev_char
        for c in s[index_count:]:
            # se o char atual eh menor que o ultimo da substring atual=ordem crescente
            if (current_sub_string.strip()[-1]) < c:
                current_sub_string += c
                # garante que o ultimo elemento de s sera adicionado se o "for" terminar
                if current_sub_string.strip()[-1] == s.strip()[-1]:
                    sub_strings.append(current_sub_string)
            else:
                sub_strings.append(current_sub_string)
                break

    # adiciona o ultimo elemento de s
    sub_strings.append(s.strip()[-1])
    return sub_strings if sub_strings else s

# retorna a primeira substring de tamanho maximo
def findLargest(s):
    if s == '':
        return ''
    sub_strings = largestIncreasingSubstr(s)
    return max(sub_strings, key=len)

# retorna lista com todas as substrings de tamanho maior que K
def findLargestThan(s, K):
    selected_subs = []
    sub_strings = largestIncreasingSubstr(s)
    for sub in sub_strings:
        if len(sub) > K:
            selected_subs.append(sub)
    return selected_subs
