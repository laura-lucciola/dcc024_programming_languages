# dcc024_programming_languages

## Authors/Autores:
https://gitlab.com/laura-vianna  

### Repository for coursework and online exams of Programming Languages module - 2017/1 

1. euler_76 - solutions to Euler 76 problem - Python3, Prolog, SML and C#   
https://projecteuler.net/problem=76   
(Partially done)  

2. online_exams - my solutions for the subject's the online finals - Python3, Prolog and SML  


### Repositorio para trabalhos e provas online da disciplina Linguagens de Programacao - 2017/1

1. euler_76 - solucoes para o problema de Euler 76 - Python3, Prolog, SML e C#   
https://projecteuler.net/problem=76  
(Feito parcialmente)  

2. online_exams - minhas solucoes para as provas finais online da disciplina - Python3, Prolog e SML   
