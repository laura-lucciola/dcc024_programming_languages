void Main()
{
	var number = 10;
    recurse(number, 1, 0, "");
	
	Console.WriteLine($"Number of results = {results.Count()}");
}

List<string> results = new List<string>();

int recurse(int n, int min, int count, string result)
{
	if (n == 0) 
	{
		// Add it to the list in a nice format so we can view it if we want
  		results.Add(string.Join(" + ", result.TrimStart().Split(' ')));
	}

	for (int x = min ; x <= n ; x++) {
  		count = recurse(n - x, x, count, result + " " + x);
  	}

  	return count;
}
