print "-------------------------Hello, World\n";

fun print_sum []      = ""
  | print_sum [x]     = Int.toString x
  | print_sum (h::t) = Int.toString h ^ " + " ^ print_sum t

(* finds the sum of elements of a list *)
fun list_sum [] = 0
| list_sum (h::t) = foldr (op +) h t

(* returns true if sum of a list L is the integer x *)
fun list_is_int(x, []) = []
| list_is_int(x, L) =  if (x = list_sum(L)) then L else []

(*makes a list [x, x-1, x-2...[]] *)
fun sub_list 0 = []
| sub_list 1 = [1]
| sub_list x = 
    let
       val temp = sub_list(x-1)
    in
       x::temp
    end



fun sum_operands ([], max) = []
| sum_operands ((h::t), max) = 
    let
       val possible = sub_list(h-1)
    in

	    let
	       val second_ps = tl(possible)
	    in
		list_is_int(max, hd(possible)::second_ps)@sum_operands(possible, max)
	    end
    end
(*
sum_operands 3,3
possible = 2,1
second_ps = 1
(3,[2]::1)@sum(2,use)

sum_operands [2,1],3
possible = 1
second_ps = []
(3,[1])@sum(2,use)

*)




(*



3 "euler_76.sml";

| sum_operands (x, [y]) = if (x = y) then [[x]] else []

fun euler_76 0 = 0
| euler_76 1 = 1
|euler_76 n:int = 
    let
       val components = sub_list(n)
    in
	if (list_is_int(n, components) = true)
	then n	 
	else euler_76 n       		
    end

fun partitions [] = []
  | partitions [x] = [[[x]]]
  | partitions (x::xs) =
    let
       val zsss = partitions xs
    in
       map (fn zss => [x]::zss) zsss @
       map (fn zs::zss => (x::zs)::zss) zsss
    end


				

fun extensions x [] = []
  | extensions x (h::t) =
    ((x::h)::t) :: map (fn zss => h::zss) (extensions x t)

fun partitions [] = [[]]
  | partitions (h::t) =
    List.concat (map (fn zss => ([h]::zss) :: extensions h zss) (partitions t))
*)
