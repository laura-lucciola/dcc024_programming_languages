print "Hello, World\n";

fun print_sum []      = ""
  | print_sum [x]     = Int.toString x
  | print_sum (h::t) = Int.toString h ^ " + " ^ print_sum t;

